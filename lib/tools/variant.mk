# CMD := $(PLANT_DIR_EXEC)/variant
# VARIANT_VERSION := 0.34.0

# ifeq (,$(wildcard $(CMD)))
#   ifeq (x86_64,$(PLANT_ARCH))
#     PLANT_ARCH := amd64
#   else
#     PLANT_ARCH := 386
#   endif

#   export PLANT_TGZ += variant:https://github.com/mumoshu/variant/releases/download/v0.34.0/variant_$(VARIANT_VERSION)_$(PLANT_OS)_$(PLANT_ARCH).tar.gz

#   ifneq (,$(DEBUG))
#     $(info tool: variant: installing)
#   endif
# else
#   ifneq (,$(DEBUG))
#     $(info tool: variant: installed)
#   endif
# endif

# ifneq (,$(PLANT_TOOLS)$(filter tar,$(PLANT_TOOLS)))
#   include tar.mk
# endif
# ifneq (,$(PLANT_TOOLS)$(filter git,$(PLANT_TOOLS)))
#   include git.mk
# endif
