CMD = $(PLANT_DIR_BIN)/unzip
export PLANT_INST += zip/install
export PLANT_ZIPS ?=

ifeq (,$(wildcard $(CMD)))
  export PLANT_PKGS += unzip
  export PLANT_CMDS += $(CMD)

  ifneq (,$(DEBUG))
    $(info tool: zip: installing)
  endif
else
  ifneq (,$(DEBUG))
    $(info tool: zip: installed)
  endif
endif

ifneq (,$(PLANT_TOOLS)$(filter curl,$(PLANT_TOOLS)))
  include curl.mk
endif

ZIP_FLAGS = -j
ifeq (,$(DEBUG))
  ZIP_FLAGS += -qq
endif

export ZIP_DIR ?= /tmp
export CURL_FILES ?=
export ZIP_FILES ?=

zip/unzip:
	@mkdir -p $(ZIP_DIR)
	@set $(PLANT_FLAGS_SET); \
	[ -z $(DEBUG) ] || echo \$${ZIP_FILES}=$${ZIP_FILES}; \
	for ZIP in $${ZIP_FILES}; \
	do \
		DIR="$(ZIP_DIR)/$$(echo $${ZIP} | awk -F. '{print $$1}')"; \
		[ -z $(DEBUG) ] || echo \$${ZIP}=$${ZIP} \$${DIR}=$${DIR}; \
		cd $${DIR} && unzip $(ZIP_FLAGS) $${ZIP}; \
	done

zip/install: CURL_FILES = $(foreach ZIP,$(PLANT_ZIPS),$(shell echo "$(ZIP)" | sed 's/\([\w\d\.]*\)\(,.*\)\?:\(.*\)\(.zip\)/\1\4:\3\4/'))
zip/install: ZIP_FILES = $(foreach ZIP,$(PLANT_ZIPS),$(shell echo "$(ZIP)" | sed 's/\([\w\d\.]*\)\(,.*\)\?:\(.*\)\(.zip\)/\1\4/'))
zip/install: FILES = $(foreach ZIP,$(PLANT_ZIPS),$(shell echo "$(ZIP)" | sed 's/\([\w\d\.]*\)\(,.*\)\?:\(.*\)\(.zip\)/\1\4/'))
zip/install: curl/download-zip zip/unzip
	@set $(PLANT_FLAGS_SET); \
	[ -z $(DEBUG) ] || echo \$${PLANT_ZIPS}=$${PLANT_ZIPS}; \
	for FILES in $${PLANT_ZIPS}; \
	do \
		FILES=$$(echo $${FILES} | sed 's/^\([^:]*\):.*/\1/') \
		ZIP="$$(echo $${FILES} | awk -F, '{ print $$1 }').zip"; \
		DIR="$(ZIP_DIR)/$$(echo $${ZIP} | awk -F. '{print $$1}')"; \
		for FILE in $${FILES}; \
		do \
			cp $(PLANT_FLAGS_CP) $${DIR}/$${FILE} $${PLANT_DIR_EXEC}; \
			chmod 755 $${PLANT_DIR_EXEC}/$${FILE}; \
		done; \
	done
