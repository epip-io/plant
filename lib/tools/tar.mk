CMD = $(PLANT_DIR_BIN)/tar
export PLANT_INST += tar/install
export PLANT_TGZ ?=

ifeq (,$(wildcard $(CMD)))
  export PLANT_PKGS += tar
  export PLANT_CMDS += $(CMD)

  ifneq (,$(DEBUG))
    $(info tool: tar: installing)
  endif
else
  ifneq (,$(DEBUG))
    $(info tool: tar: installed)
  endif
endif

ifneq (,$(PLANT_TOOLS)$(filter curl,$(PLANT_TOOLS)))
  include curl.mk
endif

ifneq (,$(DEBUG))
  TAR_FLAGS := -v --transform 's|.*/||' -xf
else
  TAR_FLAGS := --transform 's|.*/||' -xf
endif

export TAR_DIR ?= /tmp
export CURL_FILES ?=
export TGZ_FILES ?=

tar/untar:
	@mkdir -p $(TAR_DIR)
	@set $(PLANT_FLAGS_SET); \
	[ -z $(DEBUG) ] || echo \$${TGZ_FILES}=$${TGZ_FILES}; \
	for TAR in $${TGZ_FILES}; \
	do \
		DIR="$(TAR_DIR)/$$(echo $${TAR} | awk -F. '{print $$1}')"; \
		[ -z $(DEBUG) ] || echo \$${TAR}=$${TAR} \$${DIR}=$${DIR}; \
		cd $${DIR} && tar $(TAR_FLAGS) $${TAR}; \
	done

tar/install: CURL_FILES = $(foreach TAR,$(PLANT_TGZ),$(shell echo "$(TAR)" | sed 's/\([\w\d\.]*\)\(,.*\)\?:\(.*\)\(.t.*\)/\1\4:\3\4/'))
tar/install: TGZ_FILES = $(foreach TAR,$(PLANT_TGZ),$(shell echo "$(TAR)" | sed 's/\([\w\d\.]*\)\(,.*\)\?:\(.*\)\(.t.*\)/\1\4/'))
tar/install: FILES = $(foreach TAR,$(PLANT_TGZ),$(shell echo "$(TAR)" | sed 's/\([\w\d\.]*\)\(,.*\)\?:\(.*\)\(.t.*\)/\1\4/'))
tar/install: curl/download-tgz tar/untar
	@set $(PLANT_FLAGS_SET); \
	[ -z $(DEBUG) ] || echo \$${PLANT_TGZ}=$${PLANT_TGZ}; \
	for FILES in $${PLANT_TGZ}; \
	do \
		FILES=$$(echo $${PLANT_TGZ} | sed 's/^\([^:]*\):.*/\1/') \
		tar="$$(echo $${FILES} | awk -F, '{ print $$1 }').tar"; \
		DIR="$(TAR_DIR)/$$(echo $${tar} | awk -F. '{print $$1}')"; \
		for FILE in $${FILES}; \
		do \
			cp $(PLANT_FLAGS_CP) $${DIR}/$${FILE} $${PLANT_DIR_EXEC}; \
			chmod 755 $${PLANT_DIR_EXEC}/$${FILE}; \
		done; \
	done
