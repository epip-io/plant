CMD = $(PLANT_DIR_BIN)/ssh

ifeq (,$(wildcard $(CMD)))
  export PLANT_PKGS += openssh-client
  export PLANT_CMDS += $(CMD)

  ifneq (,$(DEBUG))
    $(info tool: ssh: installing)
  endif
else
  ifneq (,$(DEBUG))
    $(info tool: ssh: installed)
  endif
endif

SSH_KNOWN_HOSTS ?= /etc/ssh/ssh_known_hosts

ifneq (,$(CI_SSH_KEY))
  export INIT_SSH_KEY = $(HOME)/.ssh/$(or $(CI_SSH_KEYFILE),id_rsa)

$(INIT_SSH_KEY):
	@echo "Creating $(INIT_SSH_KEY)"; \
		mkdir -p $${HOME}/.ssh; \
		echo "$${CI_SSH_KEY}" >$(INIT_SSH_KEY); \
		chmod 600 $(INIT_SSH_KEY)

endif

ssh/keyscan: $(INIT_SSH_KEY)
	@$(call assert-set,SSH_HOST)
	@ssh-keyscan -H $(SSH_HOST) >$(SSH_KNOWN_HOSTS)
