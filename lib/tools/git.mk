CMD = $(PLANT_DIR_BIN)/git

ifeq (,$(wildcard $(CMD)))
  export PLANT_PKGS += git git-lfs 
  export PLANT_CMDS += $(CMD)

  ifneq (,$(DEBUG))
    $(info tool: git: installing)
  endif
else
  ifneq (,$(DEBUG))
    $(info tool: git: installed)
  endif
endif

ifeq (,$(filter $(PLANT_DIR_LIB)/tools/Makefile $(PLANT_DIR_LIB)/tools/ssh.mk,$(MAKEFILE_LIST)))
  include $(PLANT_DIR_LIB)/tools/ssh.mk
endif

ifneq (,$(PLANT_NETRC_MACHINE))
  export INIT_NETRC = $(HOME)/.netrc
  export NETRC

$(INIT_NETRC):
	@echo "Creating $(HOME)/.netrc"; \
		[ -z $(DEBUG) ] || echo "\$$(PLANT_NETRC_MACHINE) = $(PLANT_NETRC_MACHINE)"; \
		echo "$${NETRC}" >$(HOME)/.netrc; \
		chmod 600 $(HOME)/.netrc

endif

git/config:
	@$(call assert-set,GIT_CONFIG_KEY)
	@$(call assert-set,GIT_CONFIG_VALUE)
	@git config $(GIT_CONFIG_KEY) $(GIT_CONFIG_VALUE)

git/init: $(INIT_NETRC) $(INIT_SSH_KEY)
	@[ -d .git ] || git init

git/remote: git/init
	@$(call assert-set,GIT_REMOTE)
	@$(call assert-set,GIT_URL)
	@echo "Setting remote $(GIT_REMOTE) to $(GIT_URL)"; \
	git remote add $(GIT_REMOTE) "$(GIT_URL)"

git/fetch:
	@$(call assert-set,GIT_REMOTE)
	@$(call assert-set,GIT_REF_SPEC)
	@echo "Fetching $(GIT_REF_SPEC) from $(GIT_REMOTE) remote"; \
	git fetch $(GIT_FLAGS_FETCH) $(GIT_REMOTE) $(GIT_REF_SPEC)

git/checkout:
	@echo "Checking out $(or $(GIT_COMMIT_SHA),FETCH_HEAD)"; \
	git checkout $(or $(GIT_FLAGS_CHECKOUT),-qf) $(or $(GIT_COMMIT_SHA),FETCH_HEAD)