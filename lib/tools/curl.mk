CMD = $(PLANT_DIR_BIN)/curl

ifeq (,$(wildcard $(CMD)))
  export PLANT_PKGS += curl
  export PLANT_CMDS += $(CMD)

  ifneq (,$(DEBUG))
    $(info tool: curl: installing)
  endif
else
  ifneq (,$(DEBUG))
    $(info tool: curl: installed)
  endif
endif

CURL_FLAGS ?= -L -s -o $${DIR}/$${NAME} $${URL}
CURL_DIR ?= /tmp

curl/download-%: .FORCE
	@mkdir -p $(CURL_DIR)
	@set $(PLANT_FLAGS_SET); \
	[ -z $(DEBUG) ] || echo \$${CURL_FILES}=$${CURL_FILES}; \
	for FILE in $${CURL_FILES}; \
	do \
		NAME=$$(echo "$${FILE}" | awk -F: '{print $$1}'); \
		URL=$$(echo "$${FILE}" | sed "s/$${NAME}:\(.*\)/\1/"); \
		DIR="$(CURL_DIR)/$$(echo $${FILE} | awk -F. '{print $$1}')"; \
		[ -z $(DEBUG) ] || echo \$${NAME}=$${NAME} \$${URL}=$${URL} \$${DIR}=$${DIR}; \
		mkdir -p $${DIR}; \
		curl $(CURL_FLAGS); \
	done
