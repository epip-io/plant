CMD := $(PLANT_DIR_EXEC)/semver

ifeq (,$(wildcard $(CMD)))
  export PLANT_ZIPS += semver:https://github.com/fsaintjacques/semver-tool/archive/master.zip

  ifneq (,$(DEBUG))
    $(info tool: semver: installing)
  endif
else
  ifneq (,$(DEBUG))
    $(info tool: semver: installed)
  endif
endif

ifeq (,$(filter $(PLANT_DIR_LIB)/tools/Makefile $(PLANT_DIR_LIB)/tools/semtag.mk,$(MAKEFILE_LIST)))
  include $(PLANT_DIR_LIB)/tools/semtag.mk
endif

ifneq (,$(CI_TAG))
  export SEMVER_MAJOR := $(shell semver get major $(CI_TAG))
  export SEMVER_MINOR := $(shell semver get minor $(CI_TAG))
  export SEMVER_PATCH := $(shell semver get patch $(CI_TAG))
  export SEMVER_PREREL := $(shell semver get prerel $(CI_TAG))

endif