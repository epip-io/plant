CMD := $(PLANT_DIR_EXEC)/semtag

ifeq (,$(wildcard $(CMD)))
  export PLANT_ZIPS += semtag:https://github.com/pnikosis/semtag/archive/master.zip

  ifneq (,$(DEBUG))
    $(info tool: semtag: installing)
  endif
else
  ifneq (,$(DEBUG))
    $(info tool: semtag: installed)
  endif
endif

ifeq (,$(filter $(PLANT_DIR_LIB)/tools/Makefile $(PLANT_DIR_LIB)/tools/zip.mk,$(MAKEFILE_LIST)))
  include $(PLANT_DIR_LIB)/tools/zip.mk
endif

ifeq (,$(filter $(PLANT_DIR_LIB)/tools/Makefile $(PLANT_DIR_LIB)/tools/git.mk,$(MAKEFILE_LIST)))
  include $(PLANT_DIR_LIB)/tools/git.mk
endif

ifeq ($(CI_COMMIT_BRANCH),$(PLANT_BRANCH_MASTER))
  SEMTAG_RELEASE ?= final

endif

ifneq (,$(filter $(PLANT_BRANCH_FEATURE)% $(PLANT_BRANCH_HOTFIX)%,$(CI_COMMIT_BRANCH)))
  SEMTAG_RELEASE ?= alpha

endif

ifneq (,$(filter $(PLANT_BRANCH_RELEASE)%,$(CI_COMMIT_BRANCH)))
  SEMTAG_RELEASE ?= candidate

endif

ifneq (,$(filter $(PLANT_BRANCH_RELEASE)%,$(CI_COMMIT_BRANCH))$(findstring $(PLANT_BRANCH_RELEASE),$(CI_COMMIT_MESSAGE)))
  SEMTAG_SCOPE ?= major

endif

ifneq (,$(filter $(PLANT_BRANCH_FEATURE)%,$(CI_COMMIT_BRANCH))$(findstring $(PLANT_BRANCH_FEATURE),$(CI_COMMIT_MESSAGE)))
  SEMTAG_SCOPE ?= minor

endif

ifneq (,$(filter $(PLANT_BRANCH_HOTFIX)%,$(CI_COMMIT_BRANCH))$(findstring $(PLANT_BRANCH_HOTFIX),$(CI_COMMIT_MESSAGE)))
  SEMTAG_SCOPE ?= patch

endif

INIT_SEMTAG :=
ifneq (,$(INIT_NETRC))
  INIT_SEMTAG += $(INIT_NETRC)

endif

ifneq (,$(INIT_SSH_KEY))
  INIT_SEMTAG += $(INIT_SSH_KEY)

endif

ifneq (,$(CI_USE_SSH))
  INIT_SEMTAG += ssh/keyscan

semtag/init: SSH_HOST = $(or $(PLANT_NETRC_MACHINE),$(CI_SSH_HOST))

endif
semtag/init: $(INIT_SEMTAG)
	@exit 0

semtag: semtag/init
	@[ -z ${DEBUG} ] || (echo "\$$(SEMTAG_RELEASE) = $(SEMTAG_RELEASE)"; \
		echo "\$$(PLANT_BRANCH_MASTER) = $(PLANT_BRANCH_MASTER)"; \
		echo "\$$(PLANT_BRANCH_FEATURE) = $(PLANT_BRANCH_FEATURE)"; \
		echo "\$$(PLANT_BRANCH_HOTFIX) = $(PLANT_BRANCH_HOTFIX)"; \
		echo "\$$(CI_COMMIT_BRANCH) = $(CI_COMMIT_BRANCH)")
	@semtag $(SEMTAG_RELEASE) -fs $(SEMTAG_SCOPE)

semtag/get: semtag/init
	@semtag get

semtag/getlast: semtag/init
	@semtag getlast

semtag/getcurrent: semtag/init
	@semtag getcurrent