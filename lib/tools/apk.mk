export APK = $(strip $(shell which apk))

ifneq (,$(APK))
  export PLANT_INST += pkg/install

  APK_ADD_FLAGS := add --no-progress
  ifneq (,$(DEBUG))
    export APK_ADD_FLAGS += -vv
  else
    export APK_ADD_FLAGS += -q
  endif

apk/install: 
	@[ -z $(DEBUG) ] || printf "\$${APK}=$${APK}\n\$${APK_ADD_FLAGS}=$${APK_ADD_FLAGS}\n\$${PLANT_PKGS}=$${PLANT_PKGS}\n"; \
		$(APK) $(APK_ADD_FLAGS) $(PLANT_PKGS)
endif
