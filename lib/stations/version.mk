include $(PLANT_DIR_LIB)/tools/semtag.mk
include $(PLANT_DIR_LIB)/tools/semver.mk

ifneq (,$(CI_TAG))
  ifeq (,$(filter-out final,$(SEMTAG_RELEASE))$(SEMVER_PREREL))
    DOCKER_TAGS :=
    ifeq (true,$(PLANT_DOCKER_ALL))
      DOCKER_TAGS += $(SEMVER_MAJOR)
      DOCKER_TAGS += $(SEMVER_MAJOR).$(SEMVER_MINOR)
    endif

    DOCKER_TAGS += $(SEMVER_MAJOR).$(SEMVER_MINOR).$(SEMVER_PATCH)

    ifeq (true,$(PLANT_DOCKER_LATEST))
      DOCKER_TAGS += latest
    endif

  else
    DOCKER_TAGS := $(SEMVER_MAJOR).$(SEMVER_MINOR).$(SEMVER_PATCH)-$(SEMVER_PREREL)-$${CI_COMMIT_SHA:0:7}
  
  endif
endif

tag-commit: semtag/getlast semtag semtag/get
	@exit 0

docker-tags: semtag/get semtag/getcurrent
	@echo "Creating $${PWD}/.tags"; \
		echo "$(strip $(DOCKER_TAGS))" | \
		tr " " "," | tee $${PWD}/.tags
	