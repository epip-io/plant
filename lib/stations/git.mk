include $(PLANT_DIR_LIB)/tools/git.mk

export GIT_REMOTE ?= origin

GIT_TARGETS := git/remote git/fetch git/checkout

ifeq (only,$(CI_USE_SSH))
  export GIT_URL = $(CI_GIT_SSH_URL)
else
  export GIT_URL = $(CI_GIT_HTTP_URL)
  ifeq (push,$(CI_USE_SSH))
    export GIT_CONFIG_KEY := url."$(shell echo "$(CI_GIT_HTTP_URL)" | sed 's|.*https://\([a-zA-Z0-9\.]*\)/.*|\1|')".pushInsteadOf
    export GIT_CONFIG_VALUE := $(shell echo "$(CI_GIT_SSH_URL)" | sed 's|\(\(ssh://\)\?[a-zA-Z0-9\.\@]*\):.*|\1|')
    GIT_TARGETS := git/remote git/config git/fetch git/checkout

  endif
endif

export GIT_REF_SPEC = +$(CI_COMMIT_REF)

ifneq (,$(CI_COMMIT_BRANCH))
  export GIT_FLAGS_CHECKOUT = -b $(CI_COMMIT_BRANCH)
  export GIT_COMMIT_SHA = $(CI_COMMIT_SHA)

endif

ifneq (,$(CI_USE_SSH))
  GIT_TARGETS := ssh/keyscan $(GIT_TARGETS)

checkout: SSH_HOST = $(or $(PLANT_NETRC_MACHINE),$(CI_SSH_HOST))

endif

checkout: $(GIT_TARGETS)
	@exit 0