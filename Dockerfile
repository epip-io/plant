FROM alpine:3.10

COPY . /opt/plant

ARG DEBUG=
ARG PLANT_HOME=/opt/plant

ENV DEBUG=${DEBUG} \
    PATH=${PLANT_HOME}/bin:${PATH} \
    MAKEFILES=${PLANT_HOME}/lib/Makefile

RUN init

ENTRYPOINT [ "/usr/bin/make" ]
