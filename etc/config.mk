export PLANT_DIR_ETC := $(PLANT_HOME)/etc
export PLANT_DIR_EXEC := $(PLANT_HOME)/bin
export PLANT_DIR_LIB := $(PLANT_HOME)/lib

export PLANT_TOOLS ?=
export PLANT_PKGS ?=
export PLANT_INST ?=

export PLANT_FLAGS_SET := -o errexit -o nounset -o pipefail
ifneq (,$(DEBUG))
  export PLANT_FLAGS_CP := -v
endif

export PLANT_OS := $(shell uname -s | tr '[:upper:]' '[:lower:]')
export PLANT_ARCH := $(shell arch)

ifneq (,$(PLANT_TOOLS)$(filter git,$(PLANT_TOOLS)))
  export PLANT_TOOLS += git
endif

ifneq (,$(wildcard /etc/os-release))
  export PLANT_DISTRO := $(shell cat /etc/os-release | grep -E ^ID= | awk -F= '{print $$2}')

  ifeq (alpine,$(PLANT_DISTRO))
    export PLANT_DIR_BIN := /usr/bin
    ifneq (,$(PLANT_TOOLS)$(filter apk,$(PLANT_TOOLS)))
      export PLANT_TOOLS += apk
    endif

    export PLANT_PKG_INST := apk
	endif
endif

include $(PLANT_DIR_ETC)/ci.mk

export PLANT_COMMA := ,
export PLANT_EMPTY :=
export PLANT_SPACE := $(PLANT_EMPTY) $(PLANT_EMPTY)

.PHONY: .FORCE

.FORCE: